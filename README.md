# Spring Cloud Config

## Repository Configuration

### Setup

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/adoucour/vehicule-manager-config.git
   
### Fill in the env.properties file

1. Locate the `env.properties` file in your project at the root

2. Open the `env.properties` file using a text editor.

3. Replace the placeholders with actual values for your environment. For example:

   ```properties
    spring.datasource.url=jdbc:postgresql://address:port/database_name
    spring.datasource.username=login
    spring.datasource.password=password